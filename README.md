# Cross Platform Replication Wrapper

This is a small cross platform (and hopefully fairly general) replication wrapper (plus ~~some~~ almost no testing), most of the core code is based on [Multiplayer Game Programming: Architecting Networked Games](https://www.oreilly.com/library/view/multiplayer-game-programming/9780134034355/) - by Josh Glazer.  This project is looking to extract out the core library allowing students in the third year Multi-Player Game Development module to use it in their own games having read the relevant book chapters.

**Windows Bug / Feature**
As of 9/22 building using the cmake-gui (and VS 2022 project files) is recommended, building via Visual Studio 2022 fails due to an unescaped % in the ninja build file produced by CMake.  

**Updated 4-1-2022:** 
- Moved to using the FetchContent plugin for gtest. 
- Moved top level CMake to the root of the project (this seems to be the convention). 

**Testing 17-5-2024:**
- Tested Windows 11 / Visual Studio 2022
- SDL2-2.30.2 / SDL2_image-2.8.2

**Updated 30-5-2024:** 
- Moved to FetchContent for SDL2 / SDL2 image. 
- Moved to FetchContent by tag GoogleTest

**Updated 3-10-2024**
- Moved to FetchContent_Populate rather than MakeAvailable (CMake and Googletest)

**Updated 8-11-2024** 
- Fixed deprecated and replaced with make available ... did I not already do this. 

## For Students ##

Be sure to **fork** then **clone** with: ```git clone https://USERNAME@bitbucket.org/smu_sc_gj/crossplatformreplication.git``` please read the known-issues before starting work. 

<!--
## Plan ##

Need to write this somewhere so I don't forget short term.

Plan to add to this:
* Types of packet (means of identifying transmitted information)
* Replication Manager
  * Naieve World Replication
  * Delta based
* RPCs
* MVE for streams of objects

Remove:
* Unrelated test stuff concerning serialisation etc.

Include:
  * Commandline download and unzip with curl. 
```bash
curl -o SDL2-devel-2.30.2-VC.zip https://www.libsdl.org/release/SDL2-devel-2.30.2-VC.zip
curl -o SDL2_image-devel-2.8.2-VC.zip https://www.libsdl.org/projects/SDL_image/release/SDL2_image-devel-2.8.2-VC.zip
unzip SDL2-devel-2.30.2-VC.zip
unzip SDL2_image-devel-2.8.2-VC.zip 
```
-->
## Targets ##

### Libraries ###

**networking** - TCP/UDP networking library and supporting data structures.  This is a wrapper around BSD Sockets and WindSock which should work across platforms.

**serialisation** - Serialisation for both basic and complex types. 

**maths** - A very basic 2D maths library

**engine** - Game Engine components, also game elements such as inputs, moves etc. sent across the network.  

**replication** - Game object registration, delta world and object replication. 

**strings** - Error logging etc.

### Executable Targets ###

**client** - Game Client, note this is composed of library classes including client versions of some of the networking components such as Network Manager. 

**server** - Game Server, note this is composed of library classes including server versions of some of the networking components such as Network Manager. 

**NetworkGame_test** - Google Tests for the various components. 

## Versions ##

| Library | Platform | Version | Tested | 
| :--: | :--: | :--: | :--: |
| SDL2 | Fedora Linux | 2.30.1 | YES | 
| SDL2_image | Fedora Linux | 2.8.2-1 | YES | 
| Google Test| Fedora Linux | 1.14.0 | YES |
| SDL2 | Windows 10 | 2.30.2 | YES | 
| SDL2_image | Windows 10 | 2.8.2-1 | YES | 
| Google Test| Windows 10 | 1.14.0 | YES |
| SDL2 | Windows 11 | 2.30.2 | YES | 
| SDL2_image | Windows 11 | 2.8.2-1 | YES | 
| Google Test| Windows 11 | 1.14.0 | YES |

## Known Issues ##
1. Need to build then re-run CMake, SDL2, SDL2_Image and GoogleTest are built by the project. The install target can only be generated after the first build!
		<!-- Need to link install guide for this -->
2. Some tests won't work on the university network
	- ~~Connect -- tries to contact an external 'echo' server.~~
	- Works provided windows/linux local network services are running on the machine. 
		- [Linux](http://www.yolinux.com/TUTORIALS/LinuxTutorialNetworking.html#INET)
			- Some linux distributions no longer provide these servers, so we have to make our own. See [here](https://nmap.org/ncat/guide/ncat-simple-services.html) <!-- Glenn ... make a script for this -->
		- [Windows](https://www.windows-security.org/windows-service/simple-tcpip-services)
3. Some tests require manual input
	- Listen -- waits for a connection on port 54321
	- Echo tests for TCP/UDP
	- **CTest** integration will mark these as failed unless the info can be sent (does all the tests at once!). 
4. Install notes need to be updated, these still refer to the runtime directory ```dist/bin```
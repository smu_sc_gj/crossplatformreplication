#ifndef GRAPHICS_DRIVER_H
#define GRAPHICS_DRIVER_H

#if defined(_WIN32) || defined(VENDORED_SDL2)
    #include <SDL.h>
    #include <SDL_image.h>   
#else
    #include <SDL2/SDL.h>
    #include <SDL2/SDL_image.h>
#endif
#include <memory>

class GraphicsDriver
{
public:

	static bool StaticInit( SDL_Window* inWnd );

	static std::unique_ptr< GraphicsDriver >		sInstance;

	void					Clear();
	void					Present();
	SDL_Rect&				GetLogicalViewport();
	SDL_Renderer*			GetRenderer();

	~GraphicsDriver();

private:

	GraphicsDriver();
	bool Init( SDL_Window* inWnd );

	SDL_Renderer*			mRenderer;
	SDL_Rect				mViewport;
};

#endif // GRAPHICS_DRIVER_H

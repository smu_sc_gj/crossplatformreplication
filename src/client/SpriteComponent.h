#ifndef SPRITE_COMPONENT_H
#define SPRITE_COMPONENT_H

#if defined(_WIN32) || defined(VENDORED_SDL2)
    #include <SDL.h>
    #include <SDL_image.h>   
#else
    #include <SDL2/SDL.h>
    #include <SDL2/SDL_image.h>
#endif

#include "Texture.h"
#include "Vector3.h"

class GameObject;

class SpriteComponent
{
public:

	SpriteComponent( GameObject* inGameObject );
	~SpriteComponent();

	virtual void		Draw( const SDL_Rect& inViewTransform );

			void		SetTexture( TexturePtr inTexture )			{ mTexture = inTexture; }

			Vector3		GetOrigin()					const			{ return mOrigin; }
			void		SetOrigin( const Vector3& inOrigin )		{ mOrigin = inOrigin; }


private:

	Vector3											mOrigin;

	TexturePtr										mTexture;

	//don't want circular reference...
	GameObject*										mGameObject;
};

typedef shared_ptr< SpriteComponent >	SpriteComponentPtr;

#endif

#define SDL_MAIN_HANDLED

#if defined(_WIN32) || defined(VENDORED_SDL2)
    #include <SDL.h>
    #include <SDL_image.h>   
#else
    #include <SDL2/SDL.h>
    #include <SDL2/SDL_image.h>
#endif

#include "Engine.h"

class Client : public Engine
{
public:

	static bool StaticInit( );

protected:

	Client();

	virtual void	DoFrame() override;
	virtual void	HandleEvent( SDL_Event* inEvent ) override;

private:

	float lastTime;
	void PrintStats();
	void OutputBandWidth();
	void OutputRoundTripTime();
	void DoNetworkTiming();
};

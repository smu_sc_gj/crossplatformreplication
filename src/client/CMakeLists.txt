# Client CMake File
# * Client executable
# * clientlib library

file(GLOB CLIENT_FILES *.cpp)

list(REMOVE_ITEM CLIENT_FILES "${CMAKE_CURRENT_SOURCE_DIR}/ClientMain.cpp")

message("Client files contains" ${CLIENT_FILES})

add_library  (clientlib STATIC ${CLIENT_FILES})

# Link SDL2

# Link SDL2main
target_link_libraries(clientlib LINK_PUBLIC SDL2::SDL2main)

# Link SDL2
target_link_libraries(clientlib LINK_PUBLIC SDL2::SDL2)

# Link SDL2_image
target_link_libraries(clientlib LINK_PUBLIC SDL2_image::SDL2_image)

# Pass on VENDORED_SDL2 flag to build 
target_compile_definitions(clientlib PUBLIC "VENDORED_SDL2=${USE_VENDORED_SDL}")

#Includes only for this target - these could possibly be for all targets but...
target_include_directories (clientlib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories (clientlib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../strings)
target_include_directories (clientlib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../maths)
target_include_directories (clientlib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../networking)
target_include_directories (clientlib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../serialisation)
target_include_directories (clientlib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../engine)
target_include_directories (clientlib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../replication)

# Link the executable to the Hello library. Since the Hello library has
# public include directories we will use those link directories when building

# Client lib link libraries
target_link_libraries (clientlib LINK_PUBLIC strings)
target_link_libraries (clientlib LINK_PUBLIC maths)
target_link_libraries (clientlib LINK_PUBLIC networking)
target_link_libraries (clientlib LINK_PUBLIC serialisation)
target_link_libraries (clientlib LINK_PUBLIC engine)
target_link_libraries (clientlib LINK_PUBLIC replication)

IF (WIN32)
	add_executable(Client WIN32 ClientMain.cpp)
ELSE()
	add_executable(Client ClientMain.cpp)
ENDIF()

#-------------------
# Fix for Console 
#-------------------

if(WIN32)
   set_target_properties(Client PROPERTIES LINK_FLAGS_DEBUG "/SUBSYSTEM:CONSOLE")
   set_target_properties(Client PROPERTIES COMPILE_DEFINITIONS_DEBUG "_CONSOLE")
   set_target_properties(Client PROPERTIES LINK_FLAGS_RELWITHDEBINFO "/SUBSYSTEM:CONSOLE")
   set_target_properties(Client PROPERTIES COMPILE_DEFINITIONS_RELWITHDEBINFO "_CONSOLE")
   set_target_properties(Client PROPERTIES LINK_FLAGS_RELEASE "/SUBSYSTEM:WINDOWS  /ENTRY:mainCRTStartup")
   set_target_properties(Client PROPERTIES LINK_FLAGS_MINSIZEREL "/SUBSYSTEM:WINDOWS  /ENTRY:mainCRTStartup")
endif(WIN32)

# Link SDL2

# Link SDL2main
target_link_libraries(Client LINK_PUBLIC SDL2::SDL2main)

# Link SDL2
target_link_libraries(Client LINK_PUBLIC SDL2::SDL2)

# Link SDL2_image
target_link_libraries(Client LINK_PUBLIC SDL2_image::SDL2_image)

# Pass on VENDORED_SDL2 flag to build 
target_compile_definitions(Client PUBLIC "VENDORED_SDL2=${USE_VENDORED_SDL}")

# Link Client Lib 

target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries (Client LINK_PUBLIC clientlib)

#Includes only for this target - these could possibly be for all targets but...
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../strings)
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../maths)
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../networking)
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../serialisation)
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../engine)
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../replication)

# Link the executable to the Hello library. Since the Hello library has
# public include directories we will use those link directories when building

# Client
target_link_libraries (Client LINK_PUBLIC strings)
target_link_libraries (Client LINK_PUBLIC maths)
target_link_libraries (Client LINK_PUBLIC networking)
target_link_libraries (Client LINK_PUBLIC serialisation)
target_link_libraries (Client LINK_PUBLIC engine)
target_link_libraries (Client LINK_PUBLIC replication)

# -------------------
# INSTALL TARGET
# -------------------

message(STATUS "Source path" ${CMAKE_SOURCE_DIR})
message(STATUS "Output path" ${CMAKE_INSTALL_PREFIX})
message(STATUS "Executable output path" ${EXECUTABLE_OUTPUT_PATH})
message(STATUS "Runtime output directory" ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
message(STATUS "Library output directory" ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})

set(APP Client)

message(STATUS "Value of APP " ${APP})

# post-build copy for win32
if(WIN32 AND NOT MINGW)
	add_custom_command( TARGET ${APP} PRE_BUILD
		COMMAND if not exist .\\dist mkdir .\\dist)
	add_custom_command( TARGET ${APP} POST_BUILD
		COMMAND copy \"$(TargetPath)\" .\\dist )
endif(WIN32 AND NOT MINGW)

if(MINGW OR UNIX)
	set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/dist)
endif(MINGW OR UNIX)

install(TARGETS ${APP}
	CONFIGURATIONS Release RelWithDebInfo Debug
	RUNTIME DESTINATION ./
	)

message(STATUS "Source path" ${CMAKE_SOURCE_DIR})
message(STATUS "Output path" ${CMAKE_INSTALL_PREFIX})

install(DIRECTORY ${CMAKE_SOURCE_DIR}/src/assets
	CONFIGURATIONS Release RelWithDebInfo Debug
	DESTINATION ./)
		

if(NOT USE_VENDORED_SDL)
	if(WIN32) # not using win32 and compiling from fetch cofig. 
		message(STATUS "SDL2 is not vendored - compiled from source (Windows)")
		set_target_properties(${APP} PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_INSTALL_PREFIX}")

		file(GLOB_RECURSE SDL2_DLL_FILES  ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/*.dll)
		file(TO_CMAKE_PATH "${SDL2_DLL_FILES}" SDL2_DLL_FILES_NATIVE)

	else()
		message(STATUS "SDL2 is not vendored - compiled from source (Linux)")
		
		# Only works on platforms which support RPATH.
		# See this for Apple example: https://github.com/rhymu8354/Excalibur/blob/main/CMakeLists.txt

	    # Distribution directiory
    	# set(CMAKE_INSTALL_RPATH ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
    	# set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
    
    	# Output has correct LD_LIBRARY_PATH
    	set(CMAKE_INSTALL_RPATH_USE_LINK_PATH ON)
		
		file(GLOB_RECURSE SDL2_DLL_FILES  ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/libSDL*)
		file(TO_CMAKE_PATH "${SDL2_DLL_FILES}" SDL2_DLL_FILES_NATIVE)

	endif()

	install(FILES ${SDL2_DLL_FILES_NATIVE}
		CONFIGURATIONS Release RelWithDebInfo Debug
		DESTINATION .
	)

else() 
	if(WIN32)
	message(STATUS "SDL2 is vendored and we're on windows - did we set the SDL2_DIR etc.?")
	#	 # https://stackoverflow.com/questions/23950887/does-cmake-offer-a-method-to-set-the-working-directory-for-a-given-build-system
	#	# Ignored due to a VS issue https://developercommunity.visualstudio.com/content/problem/268817/debugger-no-longer-respects-localdebuggerworkingdi.html
		set_target_properties(${APP} PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_INSTALL_PREFIX}")
	#

		# All DLL hack. 
		file(GLOB_RECURSE SDL2_DLL_FILES  ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/*.dll)
		file(TO_CMAKE_PATH "${SDL2_DLL_FILES}" SDL2_DLL_FILES_NATIVE)

		install(FILES ${SDL2_DLL_FILES_NATIVE}
			CONFIGURATIONS Release RelWithDebInfo Debug
			DESTINATION .
		)

	else()
		message(STATUS "SDL2 is vendored and we're on linux, should be no libraries to copy for the INSTALL target?")
	endif()
endif()
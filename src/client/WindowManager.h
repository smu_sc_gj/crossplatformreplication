#ifndef WINDOW_MANAGER_H
#define WINDOW_MANAGER_H

#include <memory>

#if defined(_WIN32) || defined(VENDORED_SDL2)
    #include <SDL.h>
    #include <SDL_image.h>   
#else
    #include <SDL2/SDL.h>
    #include <SDL2/SDL_image.h>
#endif

class WindowManager
{

public:

	static bool StaticInit();
	static std::unique_ptr< WindowManager >	sInstance;

	SDL_Window*		GetMainWindow()	const	{ return mMainWindow; }

	~WindowManager();
private:
	WindowManager( SDL_Window* inMainWindow );

	SDL_Window*				mMainWindow;
};

#endif

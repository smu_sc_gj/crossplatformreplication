# -------------------
# Google Test
# -------------------

set(gtest_force_shared_crt on)

FetchContent_Declare(
	googletest
	GIT_REPOSITORY https://github.com/google/googletest
	GIT_TAG v1.14.0
	GIT_SHALLOW TRUE
	GIT_PROGRESS TRUE
	GIT_OVERRIDE_FIND_PACKAGE TRUE
)

# FetchContent_MakeAvailable(googletest)

if(NOT googletest_POPULATED)
	FetchContent_Populate(googletest)
	add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR})
endif()


include_directories(${COMMON_INCLUDES})

add_definitions(${MSVC_COMPILER_DEFS})

enable_testing()

set(PROJECT_TEST_NAME ${PROJECT_NAME_STR}_test)
file(GLOB TEST_SRC_FILES ${PROJECT_SOURCE_DIR}/src/gtest/*.cpp)

# Construct the testing executable
add_executable(${PROJECT_TEST_NAME} ${TEST_SRC_FILES})


#I'm not sure how your supposed to figure out target names, i did a cmake <tab>
#with no dependencies to get it work.
add_dependencies(${PROJECT_TEST_NAME} gtest) # google test target
add_dependencies(${PROJECT_TEST_NAME} strings) # my target(s)
add_dependencies(${PROJECT_TEST_NAME} maths) 
add_dependencies(${PROJECT_TEST_NAME} networking) 
add_dependencies(${PROJECT_TEST_NAME} serialisation) 
add_dependencies(${PROJECT_TEST_NAME} replication) 
add_dependencies(${PROJECT_TEST_NAME} engine) 

add_dependencies(${PROJECT_TEST_NAME} clientlib) 
add_dependencies(${PROJECT_TEST_NAME} serverlib) 

target_include_directories(${PROJECT_TEST_NAME} PUBLIC ${googletest_SOURCE_DIR}/googletest/include)

# this is target inside googletest
target_link_libraries(${PROJECT_TEST_NAME} PUBLIC gtest)

# this is my target under test
target_link_libraries (${PROJECT_TEST_NAME} LINK_PUBLIC strings)
target_link_libraries (${PROJECT_TEST_NAME} LINK_PUBLIC maths)
target_link_libraries (${PROJECT_TEST_NAME} LINK_PUBLIC networking)
target_link_libraries (${PROJECT_TEST_NAME} LINK_PUBLIC serialisation)
target_link_libraries(${PROJECT_TEST_NAME} LINK_PUBLIC replication)
target_link_libraries (${PROJECT_TEST_NAME} LINK_PUBLIC engine)

target_link_libraries (${PROJECT_TEST_NAME} LINK_PUBLIC clientlib)
target_link_libraries (${PROJECT_TEST_NAME} LINK_PUBLIC serverlib)

# Link SDL2main
target_link_libraries(${PROJECT_TEST_NAME} LINK_PUBLIC SDL2::SDL2main)

# Link SDL2
target_link_libraries(${PROJECT_TEST_NAME} LINK_PUBLIC SDL2::SDL2)

# Link SDL2_image
target_link_libraries(${PROJECT_TEST_NAME} LINK_PUBLIC SDL2_image::SDL2_image)

# Pass on VENDORED_SDL2 flag to build 
target_compile_definitions(${PROJECT_TEST_NAME} PUBLIC "VENDORED_SDL2=${USE_VENDORED_SDL}")

include(GoogleTest)
gtest_discover_tests(${PROJECT_TEST_NAME}
					 DISCOVERY_MODE PRE_TEST)

#-------------------
# INSTALL TARGET
#-------------------

message(STATUS "Where is everything going %s" ${CMAKE_CURRENT_BINARY_DIR})
message(STATUS "Where is everything going %s" ${PROJECT_BINARY_DIR})

set(APP ${PROJECT_TEST_NAME})

# post-build copy for win32
if(WIN32 AND NOT MINGW)
	add_custom_command(TARGET ${APP} PRE_BUILD
		COMMAND if not exist .\\dist mkdir .\\dist)
	add_custom_command(TARGET ${APP} POST_BUILD
		COMMAND copy \"$(TargetPath)\" .\\dist)
endif(WIN32 AND NOT MINGW)

if(MINGW OR UNIX)
	set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/dist)
endif(MINGW OR UNIX)

message(STATUS "Value of APP " ${APP})

install(TARGETS ${APP}
	CONFIGURATIONS Release RelWithDebInfo Debug
	RUNTIME DESTINATION ./
	)

if(WIN32)
	set_target_properties(${APP} PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_INSTALL_PREFIX}")

	file(GLOB_RECURSE GOOGLETEST_DLL_FILES ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/libg*.dll)
	file(TO_CMAKE_PATH "${GOOGLETEST_DLL_FILES}" GOOGLETEST_DLL_FILES_NATIVE)

else()

	# Only works on platforms which support RPATH.
	# See this for Apple example: https://github.com/rhymu8354/Excalibur/blob/main/CMakeLists.txt

	# Distribution directiory
	# set(CMAKE_INSTALL_RPATH ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
	# set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

	# Output has correct LD_LIBRARY_PATH
	set(CMAKE_INSTALL_RPATH_USE_LINK_PATH ON)

	file(GLOB_RECURSE SDL2_DLL_FILES ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/libg*)
	file(TO_CMAKE_PATH "${SDL2_DLL_FILES}" GOOGLETEST_DLL_FILES_NATIVE)

endif()

install(FILES ${GOOGLETEST_DLL_FILES_NATIVE}
	CONFIGURATIONS Release RelWithDebInfo Debug
	DESTINATION .
)